#!/usr/bin/env python

'''
    LAncesTools

    Copyright 2022 Davide Marnetto

    This file is part of LAncesTools.

    LAncesTools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LAncesTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

'''

from re import split,search,match,sub
from argparse import ArgumentParser,RawDescriptionHelpFormatter
import sys
import gzip
#from os import stat
from copy import deepcopy
from itertools import chain
from warnings import warn

def main():
	parser = ArgumentParser(formatter_class=RawDescriptionHelpFormatter,
		description='''
lancestools:
Converts, merges, edits, compares, uses local admixture LAF files (and for some operations also ps21.txt files from elai). 
There are several operations available, see options below, and by combining them you can do much more.''',
		epilog='''
Accepts also standard input, specified as - in place of a file, and gzipped files (but not gzipped standard input).
Please verify the results as many operations have not been tested very thoroughly.

Example:
to perform an intersection you need to start from compressed values (i.e {0,1}), if you don't have compressed values use
lancestools mod -c to obtain them. Then you do an average and compress them again. If we want we can then extract only
some individuals and use this file to mask a VCF. Therefore:

	lancestools mod -m <(lancestools mod -c 0 my.laf ) <(lancestools mod -c 0 myother.laf ) | lancestools mod -c 0.95 - | lancestools mod -S mysamplelist - \\
	| lancestools vcf -m my.vcf - > output.vcf 
''')
	parser.set_defaults(func=None)
	parser.add_argument('--debug', default=False, help='help in debugging, set traceback limit to 5',action='store_true')
	subparsers = parser.add_subparsers(help='run each of these subcommands with option -h to see their options and descriptions.')

#converting options
	helpconv='Convert several formats into LAF.'
	descriptconv=helpconv+'\nIn many cases, SNP coordinates and sample names should be provided with dedicated options or using a VCF as reference.'
	parser_convert = subparsers.add_parser('convert', help=helpconv,description=descriptconv)
	parser_convert.set_defaults(func=convert)

	p_convert = parser_convert.add_mutually_exclusive_group()
	p_convert.add_argument('-p','--pcadmix', default=None, help='Convert *.fbk files as produced by PCAdmix (https://doi.org/10.3378/027.084.0401). Expects a *.fbk followed by a *.markers file. SNP coordinates should also be provided.',nargs=2,metavar=("FBK","MARKERS"))
	p_convert.add_argument('-R','--rfmix2', default=None, help='Convert *.fb.tsv files as produced by RFMix_v2 (https://github.com/slowkoni/rfmix). Only partly tested.',metavar="FILE")
	p_convert.add_argument('-r','--rfmix', default=None, help='Convert *.ForwardBackward.txt files as produced by RFMix_v1 (https://doi.org/10.1016/j.ajhg.2013.06.020). SNP coordinates should also be provided.',metavar="FILE")
	p_convert.add_argument('-P','--ps21', dest='ps21c', default=None, help='Convert *.ps21 files, as produced by ELAI (https://doi.org/10.1534/genetics.113.160697). SNP coordinates and sample names should also be provided.',metavar="FILE")
	p_convert.add_argument('-m','--matrix', default=None, help='Convert matrix-like files with haplotypes on rows. Each field is the ancestry at each position. Accepts {\\t ,;} as field delimiter. Accepts also non-delimited formats where each digit is considered a field. Snp coordinates and sample names should also be provided.',metavar="FILE")
	p_convert.add_argument('-t','--tmatrix', default=None, help='Convert matrix-like file with positions on rows. Each field is the ancestry at each haplotype. See option -m for further details.',metavar="FILE")
	p_convert.add_argument('-T','--tped', default=None, help='Convert masked *.tped. A way to encode local ancestry in *.tped files is to multiply the individuals and set missing the snps where the local ancestry is different. Expects a *.tped followed by a *.tfam file.',nargs=2,metavar=("TPED","TFAM"))
	#infos for converting
	parser_convert.add_argument('-v','--vcf', default=None, help='VCF file with snp coordinates and sample names, necessary when input formats do not have these info',metavar="FILE")
	parser_convert.add_argument('-s','--snpinfo', default=None, help='File with snp coordinates, necessary if they are not available from VCF or input formats. The file must contain chromosome, position and snp ID in tab or space-delimited columns. The position of such columns should be specified in the option argument by colon-separated numbers following the filename, eg: snpinfo.txt:2:3:1. Leading lines starting with # will be skipped.',metavar="FILE:CHR:POS:ID")
	parser_convert.add_argument('-n','--sampleinfo', default=None, help='File with sample names, necessary if they are not available from VCF or input formats. One sample name per line.',metavar="FILE")
	parser_convert.add_argument('-a', '--ancs', help='Either a comma-separated list of ancestries present in file, or the number of ancestries (in this case they will be assumed to be 0,1,2...). Necessary when converting tped and non-annotated matrix-like files.')
	parser_convert.add_argument('-d', dest='doubledvcf', default=False, help='flag the submitted VCF as doubled (i.e. containing homozygous individuals representing a haplotype)',action='store_true')

#operations on LAF
	helpmod='Edit LAF files (merge multiple LAF, cutoff, sort and filter individuals, filter SNPs...)'
	parser_mod = subparsers.add_parser('mod', help=helpmod,description=helpmod)
	parser_mod.set_defaults(func=mod)
	p_mod = parser_mod.add_mutually_exclusive_group()

	p_mod.add_argument('-m', '--merge',action='store_true', dest='merge', default=False, help='Average across LAF. Applicable also to *.ps21 files.')
	p_mod.add_argument('-c', '--cutoff', dest='cutoff', default=None, type=float, help='Compress LAF into {0,1} values picking the maximum ancestry, with cutoff NUM. if even the maximum ancestry is below the cutoff then the result is unknown')
	p_mod.add_argument('-s', '--sort_cols', dest='sort_cols', default=False, help='sort columns lexicografically, very important before merging/comparing files with different origin',action='store_true')
	p_mod.add_argument('-r', '--reord_ancs', dest='reordancs', default=None, help='reorder ancestries according the new position given in the string: e.g. 3,1,2 put the third ancestry as first, the first as second and the second as third.', metavar='STRING')
	p_mod.add_argument('-S', '--sample_list', dest='samples', default=None, help='extracts from file only the samples in FILE (one column, one sample per row)',metavar="FILE")
	p_mod.add_argument('-i','--interpolate', dest='intp', default=None, help='Return a file with the ancestry information for the snps in FILE, subsetting or interpolating the input file. FILE should have 2 tab or space-delimited columns, chromosome and position. Multi-chromosome interpolation is not implemented yet.',metavar="FILE")

	parser_mod.add_argument('--ps21', default=False, help='The input file are *.ps21 files and not LAF, valid only for options --merge and --cutoff', action='store_true')
	parser_mod.add_argument('FILE', help='input files to edit',nargs='+', metavar="LAF_FILE")

#operations on vcfs
	helpvcf='Annotate or mask a VCF'
	parser_vcf = subparsers.add_parser('vcf', help=helpvcf,description=helpvcf)
	parser_vcf.set_defaults(func=vcf)
	p_vcf = parser_vcf.add_mutually_exclusive_group()

	p_vcf.add_argument('-m','--mask', dest='mask', default=None, help='Returns a masked version of VCF_FILE according to the LAF file in argument. The two haplotypes of each individual need to be present independently in the LAF file with suffixes _A or _B.', metavar="VCF_FILE",type=str)
	p_vcf.add_argument('-a','--annotate', dest='annot', default=None, help='Returns an annotated version of VCF_FILE according to the LAF file in argument. The two haplotypes of each individual need to be present independently in the LAF file with suffixes _A or _B. The annotation consists in an extra subfield in the genotype fields of all individuals, specified in the format as LA.', metavar="VCF_FILE",type=str)
	parser_vcf.add_argument('FILE', help='input LAF' ,nargs=1, metavar="LAF_FILE")

# proportions
	helpprop='Returns ancestry proportions.'
	descriptprop=helpprop+'\nComputes for each individual the global ancestry proportions as snp fraction belonging to each ancestry or the site-wise local ancestry proportion across haplotypes.  Accepts multiple files with same individuals treating them as independent positions (e.g. multiple chromosomes).'
	parser_prop = subparsers.add_parser('prop', help=helpprop,description=descriptprop)
	parser_prop.set_defaults(func=prop)
	p_prop = parser_prop.add_mutually_exclusive_group()

	p_prop.add_argument('-g','--gap', default=False, help='Computes for each individual the sample-wise global ancestry proportions as snp fraction belonging to each ancestry.', action='store_true')
	p_prop.add_argument('-l','--lap', default=False, help='Computes for each snp the site-wise local ancestry proportions as haplotypic fraction belonging to each ancestry.', action='store_true')
	parser_prop.add_argument('FILE', help='input files',nargs='+', metavar="LAF_FILE")

# compare	
	helpcomp='Compare 2 LAF files.' 
	descriptcomp=helpcomp+'\nThis comparison uses classification statistics and it is only meant for non-continuous (0,1) ancestry classifications. The first file is meant to be the classification while the second is the truth. All measures are SNP-based. The classified accuracy is computed as the count of true positives over all classified SNPs (i.e. which are not unknown). When comparing two different classifications, an unknown SNP in any of the 2 files will be considered as non classified, and will not count towards the classified accuracy.' #avg_sensitivity, avg_specficity, F1_score still need to be verified
	parser_comp= subparsers.add_parser('comp', help=helpcomp,description=descriptcomp)
	parser_comp.set_defaults(func=comp)
	parser_comp.add_argument('FILE', help='file to compare',nargs=2, metavar="LAF_FILE")

	args = parser.parse_args()
	sys.tracebacklimit=0
	if args.debug:
		sys.tracebacklimit=5

	if args.func:
#		for f in args.FILE:
#			assert stat(f).st_size != 0, f+" is empty"
		args.func(args)
	else:
		parser.print_help()

######################
### misc functions ###
######################

def safe_open(filename):
	if search('\.gz$', filename):
		f=gzip.open(filename,'rt')
	else:
		f=sys.stdin if filename=="-" else open(filename,'r')
	return(f)

def rou(n):
	try:
		if n.is_integer():
			return(int(n))
		else:
			return(round(n,5))
	except AttributeError:
		return(n)

def index_or_None(l,e):
	try:
		return(l.index(e))
	except ValueError:
		return(None)

def somethingleft(f,filename):
	try:
		l=next(f)
	except StopIteration:
		return(None)
	coord=match(r'[^\t]+\t[0-9]+', l)
	raise AssertionError('SNP position '+coord.group(0)+' absent from file '+filename+'.The files must contain the same SNPs.')

def maskvcfline(raw_vcff,linef,S,vcfindex):
	vcff=[sub(':.*$', '', f) for f in raw_vcff]
	maskedht=[]
	for i,x in enumerate(linef[::2]):
		for anc in range(S):
			for hap in [0,1]:
				if linef[i*2+hap][anc]==1:
					maskedht.append(vcff[vcfindex[i]].split("|")[hap])
				else:
					maskedht.append(".")
	maskedgt=["|".join(x) for x in zip(*[iter(maskedht)]*2)]
	return(vcff[:9]+maskedgt)

def annotatevcfline(vcff,linef,lafindex):
	vcff[8]=vcff[8]+":LA" #add LA to FORMAT
	ancs=[str(x.index(1)) if any([y==1 for y in x]) else "." for x in linef] #extract ancestries from laf
	ancgts=["|".join(x) for x in zip(*[iter(ancs)]*2)]+[".|."] #couples them into ancestry genotypes
	sorted_ancgts=[ancgts[i] if i!=None else ".|." for i in lafindex] #sorts them into the order present in vcf,and adds ".|." for those absent in laf
	new_gts=[":".join(x) for x in zip(vcff[9:],sorted_ancgts)] #appends to genotype fields
	return(vcff[:9]+new_gts)

def get_names(o):
	if o.vcf:
		vcf=safe_open(o.vcf)
		for l in vcf:
			if l.startswith ('#CHR'):
				vcfheader = l.strip().split() # split line into parts
				break
		if o.doubledvcf:
			names=vcfheader[9:]
		else:
			haplabels=['A','B']
			names=[ind+"_"+hap for ind in vcfheader[9:] for hap in haplabels]
		vcf.close()
	elif o.sampleinfo:
		with safe_open(o.sampleinfo) as namef:
			names = [line.rstrip() for line in namef]
	else:
		raise RuntimeError('missing sample names, provide them with options -v|-n')
	return(names)

def get_coords(o):
	if o.vcf:
		coordf=safe_open(o.vcf)
		for l in coordf:
			if l.startswith ('#CHR'):
				break

		coords=[line.split()[0:3] for line in coordf]
	elif o.snpinfo:
		try:
			filename,*coordf=o.snpinfo.split(':')
			chrf,posf,idf=[int(x)-1 for x in coordf]
		except ValueError:
			raise RuntimeError('snp coordinates shouls be specified as "FILE:CHR:POS:ID" where CHR,POS,ID are integers identifying the field containing chromosome, position and snpID respectively.')
		with safe_open(filename) as coordf:
			for l in coordf:
				if l.startswith('#'):
					continue
				else:
					break
			coords=[]
			for line in chain([l],coordf):
				x = split('[ \t]',line.strip())
				coords.append([x[chrf],x[posf],x[idf]])
	else:
		raise RuntimeError('missing snp coordinates, provide them with options -v|-s')
	try:
		int(coords[0][1])
	except ValueError:
		coords=coords[1:]
	return(coords)

def laffify(values,ancs):
	tuples=[]
	for j in values:
		v=[0]*len(ancs)
		try:
			v[ancs.index(j)]=1
		except ValueError:
			raise RuntimeError("ancestry "+str(j)+" is not among the declared ancestries "+str(ancs))
		tuples.append(' '.join([str(x) for x in v]))
	return(tuples)

class Lafs:
	def __init__(self, files, ps21=False,canbeps21=False):
		#opening files
		infiles= [None] * len(files)
		for i,f in enumerate(files):
			infiles[i]= safe_open(f)
		# parsing laf/ps21 files
		firstline=next(infiles[0])
		if ps21:
			assert canbeps21, 'this operation cannot be run on ps21 format'
			assert not match('#CHR',firstline), 'ps21 format declared but header found (ps21 has no header)'
			self.splitline=self.split_ps21
			self.sep="  "
			self.header=None
		else:
			assert match('#CHR',firstline), 'no header found'
			self.splitline=self.split_laf
			self.sep="\t"
			self.header=firstline.rstrip()
			for infile in infiles[1:]:
				otherheader=next(infile).rstrip()
				assert self.header==otherheader, "headers of two or more files differ!"
			firstline=next(infiles[0])

		coord,f=self.parse_s(firstline)
		infiles[0]=chain([firstline],infiles[0])
		self.N=len(f)
		self.S=len(f[0].split(" "))
		self.ancs=[str(x) for x in range(self.S)]
		for i in range(1,len(infiles)):
			firstline=next(infiles[i])
			coord,f=self.parse_s(firstline)
			assert len(f)==self.N, "field counts of two or more files differ!"
			assert len(f[0].split(" "))==self.S, "ancestry counts of two or more files differ!"
			infiles[i]=chain([firstline],infiles[i])
		self.f=infiles
	

	def split_ps21(self,line):
		linef=line.rstrip().split(self.sep)
		return((None,linef))

	def split_laf(self,line):
		chrom,pos,*linef=line.rstrip().split(self.sep)
		return((chrom+"\t"+pos,linef))
	
	#each individual is read as string
	def parse_s(self,line):
		return(self.splitline(line))

	#each ancestry is read as list of numbers
	def parse_l(self,line):
		l=self.splitline(line)
		linef=[float(y) for x in l[1] for y in x.split()]
		return((l[0],linef))

	#each individual is read as tuple of numeric ancestries
	def parse_t(self,line):
		l=self.splitline(line)
		linef=[list(map(float,x.split())) for x in l[1]]
		return((l[0],linef))

################################
######      CONVERT       ######
################################
def convert(o):
	sep='\t'
	if o.ancs:
		try:
			ancs=[str(x) for x in range(int(o.ancs))]
		except ValueError:
			ancs=o.ancs.split(",")
		S=len(ancs)
	if o.doubledvcf:
		assert o.vcf, "option -d,--doubled_vcf only available when vcf is provided"

	if o.pcadmix:
		# convert pcadmix output files into laf
		# Code written by Burak Yelmen with some modifications by Davide Marnetto
		print('converting pcadmix output to laf',file=sys.stderr,flush=True)
		if o.sampleinfo:
			warn('Warning: pcadmix already has sample names: option -n|--sampleinfo will be ignored')
		if o.ancs:
			warn('Warning: pcadmix already has ancestry information: option -a|--ancs will be ignored')

		import pandas as pd
		import numpy as np
		from math import isnan
		from collections import OrderedDict

		pd.options.display.float_format = '{:.4f}'.format

		##Read file markers
		#markers = pd.read_csv(o.pcadmix[1], sep='[ \t]', header = None, engine='python')
		#markers = markers.drop(markers.columns[0], axis=1)
		#if not markers.iloc[0,-1]:
		#	markers = markers.iloc[:, :-1] #Drop last column if empty
		#markers.columns = list(range(0,len(markers.columns)))
		#window_size = len(markers.iloc[0,]) #Window size
		with safe_open(o.pcadmix[1]) as markersfile:
			markers=[split('[ \t]',l.rstrip())[1:] for l in markersfile]
		window_size=[len(x) for x in markers]

		##Read file fbk 
		fbk = pd.read_csv(o.pcadmix[0], sep='[ \t]', header = None, engine='python')
		fbk.sort_values([0,1],inplace=True)
		fbk = fbk.transpose() #Transposition
		S = len(set(fbk.iloc[1])) #Number of ancestries
		print('convert --pcadmix: ancestries represented as follows:\n'+'\n'.join([str(x)+": "+fbk.iloc[1,x] for x in range(S)]),file=sys.stderr, flush=True)
		fbk = fbk.drop(fbk.index[[1]]) #Drop ancestry row
		if isnan(fbk.iloc[-1,0]):
			fbk = fbk[:-1] #Drop last row if it's nan
		fbk = fbk.reset_index(drop=True)

		##Get positions for markers
		coords=get_coords(o)
		mapcoords={x[2]:[x[0],x[1]] for x in coords}
		marker_coords = []
		#last_window_size = len(markers.columns)

		#for x in range(0,len(markers)):
		#	for i,marker_id in enumerate(markers.iloc[x,]):
		#		try:
		#			marker_coords.append(mapcoords[marker_id])
		#		except KeyError:
		#			if pd.isnull(marker_id) and x == len(markers)-1:
		#				last_window_size = i
		#				break #Break if last row is less than window size
		#			else:
		#				raise RuntimeError("coordinates not available for "+str(marker_id))
		for marker_window in markers:
			for marker_id in marker_window:
				try:
					marker_coords.append(mapcoords[marker_id])
				except KeyError:
					raise RuntimeError("coordinates not available for "+str(marker_id))

		indivs = list(OrderedDict.fromkeys(fbk.iloc[0])) #Individual names

		##Some sloppy operations
		#fbk_last_line = fbk.iloc[[-1]]
		#fbk = fbk.iloc[1:-1]
		fbk = fbk.iloc[1:]
		fbk = pd.DataFrame(np.repeat(fbk.values,window_size,axis=0))
		#fbk = fbk.append(pd.DataFrame(np.repeat(fbk_last_line.values,last_window_size,axis=0)), ignore_index=True)
		fbk = fbk.values
		fbk = np.around(fbk.astype(np.double), 5)

		##Write to output
		print(sep.join(["#CHR","POS"] + indivs)) #print header with individual names
		for i in range(0, len(marker_coords)):
			outlist=map(str, fbk[i,])
			list2print=marker_coords[i] + [" ".join(x) for x in zip(*[iter(outlist)]*S)]
			print(sep.join(list2print))
	
	elif o.rfmix2:
		# convert rfmix (version 2) output fb.tsv files into laf
		print('converting rfmix2 *.fb.tsv to laf',file=sys.stderr,flush=True)
		with safe_open(o.rfmix2) as fbfile:
			S=len(next(fbfile).strip().split(sep))-1
			header=next(fbfile).strip().split(sep)
			haplabels=['A','B']
			indivs=[ind+"_"+hap for ind in header[4:] for hap in haplabels]
			print(sep.join(["#CHR","POS"] + indivs))
			for line in fbfile: 
				linef=line.strip().split(sep)
				assert len(indivs) == (len(linef)-4), "sample count in header and body does not match"
				print(sep.join(linef[:2]+linef[4:]))

	elif o.rfmix:
		# convert rfmix (version 1) output files into laf
		print('converting rfmix *.ForwardBackward.txt to laf',file=sys.stderr,flush=True)
		assert o.ancs, 'provide ancestry count with -a option, see lancestools convert -h'
		coords=get_coords(o)
		indivs=get_names(o)
		#print header with individual names
		print(sep.join(["#CHR","POS"] + indivs))
		with safe_open(o.rfmix) as fbfile:
			for i,line in enumerate(fbfile): #assert coords match
				linef=line.strip().split()
				assert len(indivs) == len(linef)/S, "provided sample names and sample count differ"
				outlist=[" ".join(x) for x in zip(*[iter(linef)]*S)]
				coord=coords[i]
				print(sep.join([str(x) for x in coord[0:2]]+outlist))
		assert len(coords) == i+1, "provided coordinates and variant count differ"

	elif o.ps21c:
		# convert ps21 elai output files into laf
		print('converting ps21 to laf',file=sys.stderr,flush=True)
		if o.ancs:
			warn('Warning: ps21 already has ancestry count information: option -a|--ancs will be ignored')
		import pandas as pd
		
		coords=get_coords(o)
		indivs=get_names(o)
		#print header with individual names
		print(sep.join(["#CHR","POS"] + indivs))
		#read ps21 in numpy dataframe
		ps21 = pd.read_csv(o.ps21c, sep='  ', header = None, engine='python')
		ps21 = ps21.values
		assert len(indivs) == ps21.shape[0], "provided sample names and sample count differ"
		assert len(coords) == ps21.shape[1], "provided coordinates and variant count differ"
		#Write to output
		for i,coord in enumerate(coords):
			outlist=list(ps21[:,i])
			print(sep.join([str(x) for x in coord[0:2]]+outlist))

	elif o.matrix or o.tmatrix:
		#this is to convert admix-simu true file to laf
		print('converting matrix file to laf',file=sys.stderr,flush=True)
		assert o.ancs, 'provide ancestries with -a option, see lancestools convert -h'
		coords=get_coords(o)
		indivs=get_names(o)
		header='\t'.join(['#CHR','POS']+indivs)
		matfilename=o.matrix if o.matrix else o.tmatrix
		with safe_open(matfilename) as matfile:
			l=next(matfile).rstrip()
			lsep=match('[^\t ,;]+([\t ,;])',l)
			try:
				insep=lsep.group(1)
			except AttributeError:
				insep=None
				ll=list(l)
			else:
				ll=l.split(insep)
			if o.matrix:
				assert len(coords) == len(ll), "coordinates and variant count differ"

				import pandas as pd
				import numpy as np

				if insep:
					mat = pd.read_csv(matfile, sep=insep, header = None, engine='python',dtype=str)
				else:
					mat = pd.read_fwf(matfile, widths=[1]*len(ll),header=None,dtype=str)
				mat = mat.values
				mat=np.vstack((ll,mat))
				assert len(indivs) == mat.shape[0], "sample names and sample count differ"
				#Write to output
				print(header)
				for i,coord in enumerate(coords):
					values=list(mat[:,i])
					outlinef=laffify(values,ancs)
					print(sep.join(coord[0:2]+outlinef))

			elif o.tmatrix:
				assert len(indivs) == len(ll), "sample names and sample count differ"
				print(header)
				for i,line in enumerate(matfile):
					if insep:
						values=line.strip().split(insep)
					else:
						values=list(line.strip())
					coord=coords[i]
					outlinef=laffify(values,ancs)
					print(sep.join(coord[0:2]+outlinef))
				assert len(coords) == i+1, "provided coordinates and variant count differ"

	elif o.tped:
		#this is to convert tped to laf... a way to display local ancestry results is from a masked tped (missing genotypes encoded as 0)
		print('converting tped to laf',file=sys.stderr,flush=True)
		assert o.ancs, 'provide ancestry count with -a option, see lancestools convert -h'
		if o.sampleinfo:
			warn('Warning: tped already has sample names: option -n|--sampleinfo will be ignored')
		if o.snpinfo:
			warn('Warning: tped already has positions: option -s|--snpinfo will be ignored')
		inds=[]
		tfam=safe_open(o.tped[1])
		for line in tfam:
			f = split('[\t ]+',line.rstrip())
			inds.append(f[1])
		tfam.close()
		N=len(inds)
		order=list(range(N))
		order.sort(key=lambda x: inds[x])
		orderedinds=[inds[x] for x in order]
		for i in range(N)[::S]:
			orderedinds[i]='_'.join(orderedinds[i].split('_')[:-1])
			for j in range(1,S):
				assert orderedinds[i]=='_'.join(orderedinds[i+j].split('_')[:-1]), orderedinds[i]+" does not have "+str(S)+" ancestries"
		header=['#CHR','POS']+orderedinds[::S]
		print(sep.join(header))
		tped=safe_open(o.tped[0])
		for line in tped:
			f=split('[\t ]+',line.rstrip())
			vals=[]
			#grep -v D I
			for i in range(len(inds)):
				s= " " if i%2==0 else sep
				vals.append("0"+s if f[order[i]*2+4]=="0" else "1"+s)
			print(f[0]+sep+f[3]+sep+"".join(vals).rstrip())
		tped.close()

############################
###### VCF operations ######
############################
def vcf(o):
	#masks or annotates a vcf file according to a laf file
	assert len(o.FILE)==1, 'only one laf file accepted'
	la=Lafs(o.FILE)
	vcfname= o.mask if o.mask else o.annot
	inds=la.header.split(la.sep)[2:]
	hapinds=deepcopy(inds)
	inds=[i[:-2] for i in hapinds[::2]]
	with safe_open(vcfname) as vcf:
		for line in vcf:
			if line.startswith('#CHR'):
				vcfheader = line.rstrip().split("\t") # split line into parts
				if o.annot:
					print('''##FORMAT=<ID=LA,Number=1,Type=String,Description="Local Ancestry status">''')
				break
			else:
				print(line,end="",flush=True)
		if o.mask:
			print('masking vcf according to laf',file=sys.stderr,flush=True)
			vcfindex=[vcfheader.index(i) for i in inds]
			maskedinds=[x+"_"+str(y) for x in inds for y in list(range(1,la.S+1))]
			print("\t".join(vcfheader[:9]+maskedinds))
		else:
			print('annotating vcf according to laf',file=sys.stderr,flush=True)
			lafindex=[index_or_None(inds,i) for i in vcfheader[9:]]
			print("\t".join(vcfheader))
		for line in la.f[0]:
			coord,linef=la.parse_t(line)
			assert all([x.is_integer() for t in linef for x in t]), "non-binary {0,1} ancestry classifications found. Apply cutoff before using as mask."
			chrom,pos=coord.split("\t")
			vcff=next(vcf).rstrip().split("\t")
			assert chrom==vcff[0] and pos==vcff[1], "SNP positions "+vcff[0]+" "+vcff[1]+" (vcf) and "+chrom+" "+pos+" (laf) differ but at same line when dealing with vcf. The files must contain the same SNPs." 
			if o.mask:
				newvcfline=maskvcfline(vcff,linef,la.S,vcfindex)
			else:
				newvcfline=annotatevcfline(vcff,linef,lafindex)			
			print("\t".join(newvcfline))



###########################
### modifying laf files ###
###########################
def mod(o):
	if o.merge:
		#merges more laf/ps21 files by computing an average
		print("merging files",file=sys.stderr,flush=True)
		assert len(o.FILE)>1, 'multiple laf files are needed'
		la=Lafs(o.FILE,o.ps21,canbeps21=True)
		if la.header:
			print(la.header)
		for line in la.f[0]:
			coord,SUMlinef=la.parse_l(line)
			for i in range(1,len(la.f)):
				try:
					line=next(la.f[i])
				except StopIteration:
					raise AssertionError('SNP position '+coord+' absent from file '+o.FILE[i]+'.The files must contain the same SNPs.')
				newcoord,linef=la.parse_l(line)
				assert coord==newcoord, "SNP positions "+coord+" and "+newcoord+" differ but at same line when merging. The files must contain the same SNPs."
				SUMlinef=map(lambda x,y: x+y,SUMlinef,linef)
			AVGlinef=[x/len(la.f) for x in SUMlinef]
			outlist=[str(rou(x)) for x in AVGlinef]
			if coord:
				list2print=[coord]+[" ".join(x) for x in zip(*[iter(outlist)]*la.S)]
			else:
				list2print=[" ".join(x) for x in zip(*[iter(outlist)]*la.S)]
			print(la.sep.join(list2print))

		for i in range(1,len(la.f)):
			somethingleft(la.f[i],o.FILE[i])

	elif o.cutoff != None:
		# turns continuous ancestry values into 0,1 by applying a cutoff
		print("compressing with cutoff:"+str(o.cutoff),file=sys.stderr,flush=True)
		assert len(o.FILE)==1, 'only one laf file accepted'
		la=Lafs(o.FILE,o.ps21,canbeps21=True)
		if la.header:
			print(la.header)
		for line in la.f[0]:
			coord,linef=la.parse_t(line)
			ancmax=[x.index(max(x)) for x in linef]
			outlinef=[0]*(la.S*la.N)
			for t,i in list(zip(enumerate(linef),ancmax)):
				if t[1][i]>=o.cutoff:
					outlinef[t[0]*la.S+i]=1
			outlist=[str(x) for x in outlinef]		
			if coord:
				list2print=[coord]+[" ".join(x) for x in zip(*[iter(outlist)]*la.S)]
			else:
				list2print=[" ".join(x) for x in zip(*[iter(outlist)]*la.S)]
			print(la.sep.join(list2print))

	elif o.reordancs:
		# reorder ancestry values for each individual
		print("reordering ancestries as "+str(o.reordancs),file=sys.stderr,flush=True)
		assert len(o.FILE)==1, 'only one laf file accepted'
		la=Lafs(o.FILE,o.ps21,canbeps21=True)
		if la.header:
			print(la.header)
		neworder=[int(x)-1 for x in o.reordancs.split(",")]
		newS=len(neworder)
		for line in la.f[0]:
			coord,linef=la.parse_t(line)
			outlist=[str(rou(x[i])) for x in linef for i in neworder]
			if coord:
				list2print=[coord]+[" ".join(x) for x in zip(*[iter(outlist)]*newS)]
			else:
				list2print=[" ".join(x) for x in zip(*[iter(outlist)]*newS)]
			print(la.sep.join(list2print))

	elif o.sort_cols:
		# sorts the columns of a laf file so that the names of the samples are in lexicographic order
		print("sorting columns",file=sys.stderr,flush=True)
		assert len(o.FILE)==1, 'only one laf file accepted'
		la=Lafs(o.FILE,o.ps21)
		inds=la.header.split(la.sep)[2:]
		order=list(range(len(inds)))
		order.sort(key=lambda x: inds[x])
		orderedinds=[inds[x] for x in order]
		header=['#CHR','POS']+orderedinds
		print(la.sep.join(header))
		for line in la.f[0]:
			coord,linef=la.parse_s(line)
			orderedlinef=[coord]+[linef[x] for x in order]
			print(la.sep.join(orderedlinef))

	elif o.samples:
		# extract samples
		print("extracting samples",file=sys.stderr,flush=True)
		assert len(o.FILE)==1, 'only one laf file accepted'
		la=Lafs(o.FILE,o.ps21)
		inds=la.header.split(la.sep)[2:]
		with safe_open(o.samples) as f:
			samplist=f.read().splitlines()
		idxlist=[inds.index(x) for x in samplist]
		header=['#CHR','POS']+[inds[x] for x in idxlist]
		print(la.sep.join(header))
		for line in la.f[0]:
			coord,linef=la.parse_s(line)
			outlinef=[coord]+[linef[x] for x in idxlist]
			print(la.sep.join(outlinef))

	elif o.intp:
		print("interpolating over positions in file:"+o.intp,file=sys.stderr,flush=True)
		assert len(o.FILE)==1, 'only one laf file accepted'
		la=Lafs(o.FILE,o.ps21)
		with safe_open(o.intp) as iposfile:
			for l in iposfile:
				if l.startswith('#'):
					continue
				else:
					break
			chrom,iposraw=split('[ \t]',l.strip())
			line=next(la.f[0]).rstrip()
			coord=match(r'([^\t]+)\t([0-9]+)', line)
			assert coord.group(1)==chrom, o.FILE[0]+" and "+o.intp+": chromosome differ!"

			pline=None
			pos=int(coord.group(2))
			ipos=int(iposraw)
			print(la.header)
			while True:
				if ipos == pos:
					print(line)
					try:
						ipos=int(split('[ \t]',next(iposfile).strip())[1])
					except StopIteration:
						break

				elif ipos < pos:
					if pline and line:
						pcoord,plinef=la.parse_l(pline)
						coord,linef=la.parse_l(line)
						w=(ipos-ppos)/(pos-ppos)
						ilinef=[pv*(1-w)+v*w for pv,v in zip(plinef,linef)]
						ilinef=[str(rou(x)) for x in ilinef]
					else:
						ilinef=['0']*la.S*la.N
					list2print=[chrom,str(ipos)]+[" ".join(x) for x in zip(*[iter(ilinef)]*la.S)]
					print(la.sep.join(list2print))
					try:
						ipos=int(split('[ \t]',next(iposfile).strip())[1])
					except StopIteration:
						break

					continue

				pline=line
				ppos=int(match(r'[^\t]+\t([0-9]+)', pline).group(1))
				try:
					line=next(la.f[0]).rstrip()
					pos=int(match(r'[^\t]+\t([0-9]+)', line).group(1))
				except StopIteration:
					line=None
					pos=float('inf')
	else:
		raise RuntimeError("At least one option must be choosen for lancestools mod, see lancestools mod -h")



def comp(o):
	print("Comparing file "+o.FILE[0]+" with reference file "+o.FILE[1],file=sys.stderr,flush=True)
	import numpy as np
	la=Lafs(o.FILE)
	om=np.zeros(shape=(la.N,la.S+1,la.S+1),dtype=int) #initialize a 3d matrix where the first coordinate is the individual index
	for line in la.f[0]:
		coord,values=la.parse_t(line)
		assert all([x.is_integer() for t in values for x in t]), "non-binary {0,1} ancestry classifications found. Apply cutoff before comparing"
		try:
			othercoord,othervalues=la.parse_t(next(la.f[1]))
		except StopIteration:
			raise AssertionError('SNP position '+coord+' absent from file '+o.FILE[1]+'.The files must contain the same SNPs.')
		assert coord==othercoord, "SNP positions "+coord+" and "+othercoord+" differ but at same line when comparing. The files must contain the same SNPs."
		for i,ind in enumerate(values): #for each individual, store the snps in a confusion matrix 
			try:
				x=values[i].index(1)
			except ValueError:
				x=la.S
			try:
				y=othervalues[i].index(1)
			except ValueError:
				y=la.S
			om[i,x,y]+=1 
			# the 2nd and 3rd coordinates are the ancestry according to file1 and file2, respectively
			# if file1 is prediction and file2 is reality then
			# the 2nd coordinate is the ancestry prediction and the 3rd coordinate is the real ancestry status
	somethingleft(la.f[1],o.FILE[1])

	##Compute statistics and print output

	#print('#IND\tavg_sensitivity\tavg_specficity\tF1_score\taccuracy\taccuracy_classified\tfrac_classified\tN')
	print('#IND\taccuracy\taccuracy_classified\tfrac_classified\tN')

	inds=la.header.split('\t')[2:]
	for i,ind in enumerate(inds):

		#we report for now only accuracy, accuracy_classified and fraction classified, so these stats are commented
		#statistics=[0,0,0] #statistics are [sensitivity,specificity,precision]
		#for x in range(0,la.S): #compute them for each ancestry
		#	tp=om[i,x,x]
		#	t=np.sum(om[i,:,x]) # 2nd coordinate is ancestry in file1, omitting it means all trues
		#	p=np.sum(om[i,x,:]) # 3rd coordinate is ancestry in file2, omitting it means all positives
		#	f=np.sum(om[i,:,:])-t #total - trues means all falses
		#	tn=f-p+tp #falses - false positives means all true negatives
		#	statistics[0] += tp/t if t>0 else 0 #sensitivity=tp/(tp+fn)=tp/t
		#	statistics[1] += tn/f if f>0 else 0 #specificity=tn/(tn+fp)=tn/f
		#	statistics[2] += tp/p if p>0 else 0 #prec=tp/(tp+fp)=tp/p
		#avg_sens,avg_spec,avg_prec = [x/la.S for x in statistics] #take the average
		#F1score = 2*(avg_sens**-1+avg_prec**-1)**-1 if avg_sens!=0 and avg_prec!=0 else 0.0 #F1_score=harm_mean(avg_prec,avg_sens)

		tp= np.sum([om[i,x,x] for x in range(0,la.S)]) #true positives across ancestries
#		tot= np.sum(om[i,:,:]) #total elements in confusion matrix, abandoned to contemplate cases with incomplete reference ancestries.
		tot= np.sum(om[i,0:la.S,:]) #total elements in confusion matrix with a reference classification. elements without a reference classification are ignored.
		if tot==0:
			print("\t".join([ind]+["NA","NA","NA","0"]))
			continue
		totclass= np.sum(om[i,0:la.S,0:la.S]) #total of classified elements
		acc=tp/tot #acc=tp/tot
		accclass=tp/totclass if totclass!=0 else 0.0 #accuracy considering only classified elements
		fracclass=totclass/tot #fraction of classified elements

		#print("\t".join([ind]+[str(rou(x)) for x in [avg_sens,avg_spec,F1score,acc,accclass,fracclass,tot]]))
		print("\t".join([ind]+[str(rou(x)) for x in [acc,accclass,fracclass,tot]]))


def prop(o):
	la=Lafs(o.FILE)
	if o.gap:
		print("Computing sample-wise global ancestry proportion",file=sys.stderr,flush=True)
		inds=la.header.split('\t')[2:]
		coord,outsum=la.parse_l(next(la.f[0]))
		n=1
		for f in la.f:
			for l in f:
				n=n+1
				coord,linef=la.parse_l(l)
				outsum=[sum(x) for x in zip(outsum,linef)]
		#print output
		print('#IND\t'+'\t'.join(la.ancs)+'\tunknown\tN')
		for idx,ind in enumerate(inds):
			ancs=[x/n for x in outsum[idx*la.S:idx*la.S+la.S]]
			unkn=1-sum(ancs)
			print(la.sep.join([ind]+[str(rou(x)) for x in ancs+[unkn]+[n]]))
	elif o.lap:
		print("Computing snp-wise local ancestry proportion",file=sys.stderr,flush=True)
		print('#CHR\tPOS\t'+'\t'.join(la.ancs)+'\tunknown\tN')
		for f in la.f:
			for l in f:
				coord,linef=la.parse_t(l)
				ancs=[0]*la.S
				n=len(linef)
				for ind in linef:
					ancs=[x+y for x,y in zip(ancs,ind)]
				ancs=[x/n for x in ancs]
				unkn=1-sum(ancs)
				print(la.sep.join([coord]+[str(rou(x)) for x in ancs+[unkn]+[n]]))
	else:
		raise RuntimeError("At least one option must be choosen for lancestools prop, see lancestools prop -h")

if __name__ == '__main__':
	main()


