# LAncesTools #

__LAncesTools__ is a suite of shell power tools  written in Python to deal with Local Ancestry information. In particular it deals with Local Ancestry Format (LAF) files (see below for decription and considerations) and for some operations also ps21.txt files from ELAI. The amount of operations that you can do is pretty high: converting formats, sorting and extracting samples and ancestries, interpolating on different SNPs, applying cut-offs, computing global proportions, compare local ancestry inferences, etc ... Furthermore, the UNIX-like approach adopted by LAncesTools (which has been applied in BCFtools as well) allows countless combinations.

The tool was created initially for internal use, therefore, while it looks quite robust for common situations, it did not undergo systematic testing. Use it with care and if you notice something fishy, feel free to email me at davide.marnetto@unito.it. 

## Acknowledgements ##

I would like to thank:

- __Burak Yelmen__ for contributing in the early code to convert from PCAdmix format and for testing.
- __Ludovica Molinaro__, __Francesco Montinaro__ and __Luca Pagani__ for crucial testing and feedback.

## Citation ##

If you use __LAncesTools__ for some work that ends up in a publication and you want to acknowledge its use, please cite one of the two papers LAncesTools was created for:

- Marnetto, D., Pärna, K., Läll, K., Molinaro, L., Montinaro, F., Haller, T., Metspalu, M., Mägi, R., Fischer, K., & Pagani, L. (2020). Ancestry deconvolution and partial polygenic score can improve susceptibility predictions in recently admixed individuals. Nature communications, 11(1), 1628. [https://doi.org/10.1038/s41467-020-15464-w](https://doi.org/10.1038/s41467-020-15464-w)

- Molinaro, L., Marnetto, D., Mondal, M., Ongaro, L., Yelmen, B., Lawson, D. J., Montinaro, F., & Pagani, L. (2021). A Chromosome-Painting-Based Pipeline to Infer Local Ancestry under Limited Source Availability. Genome biology and evolution, 13(4), evab025. [https://doi.org/10.1093/gbe/evab025](https://doi.org/10.1093/gbe/evab025)

## Installation ##

Download or clone the present repository (`git clone git@bitbucket.org:dmarnetto/lancestools.git`). In the downloaded directory you will see a single python script (`lancestools.py`), plus some examples.
You can run LAncesTools as long as you have python 3 installed and this script or a link to it is added to one of your $PATH directories. E.g.:
```
cp lancestools.py /my_software/src
cd /usr/local/bin
ln -s /my_software/src/lancestools.py lancestools
```
LAncesTools was written and tested with python 3.7 - 3.9.

## LAF FIles ##

LAF is a tab- and space-delimited file format which contains all Local Ancestry information without the need of helper files. It is inspired by VCF, and similar to the fb.tsv files from RFMix2. LAFs have the following features:

- a single-line header, prepended by #.

- two tab-delimited fixed columns: #CHR and POS.

- one tab-delimited column for each sample, which is a haploid genome or half an individual. Samples are usually labeled as samplex_A and samplex_B for a complete diploid individual.

- within each sample column, one space-delimited field for each of the ancestries represented, with anumber representing the probability of that ancestry being the true one. For some uses these numbers are required to be either 1 or 0.

See below an example of LAF file:

```
#CHR	POS	NA19703_A	NA19703_B	NA19704_A	NA19704_B
1	1823922	0.94 0 0.6	0.96 0.01 0.03	0.94 0 0.6	0.96 0.01 0.03
1	2291680	0.96 0.01 0.03	0.96 0.01 0.03	0.96 0.01 0.03	0.96 0.01 0.03
1	2390331	0.85 0.10 0.05	0.85 0.10 0.05	0.85 0.10 0.05	0.85 0.10 0.05
1	2490898	0.85 0.10 0.05	0.96 0.01 0.03	0.85 0.10 0.05	0.96 0.01 0.03
```

Please note that LAncesTools contains an utility to convert from several formats to LAF. Right now LAncesTools can just convert from RFMix2 fb.tsv to LAF (tested on example1.fb.tsv), but you might have noticed that the fb.tsv files differ only for the header and 2 extra fixed columns. In case many users are interested in using fb.tsv files I might consider to update LAncesTools to use it natively and abandon LAF altogether. In case you would be interested, drop me an email (davide.marnetto@unito.it).

## Examples ##

You can try out the following examples by placing yourself in the examples directory.

```
$ lancestools mod -c 0.9 example1.laf \
| lancestools mod -r 1,3 - \
| lancestools mod -S <(echo -e "NA19704_B\nNA19704_A") - \
| lancestools mod -i <(head pos_interpolate.txt) - \
| lancestools mod -s -

sorting columns
reordering ancestries as 1,3
interpolating over positions in file:/dev/fd/63
extracting samples
compressing with cutoff:0.9
#CHR	POS	NA19704_A	NA19704_B
1	1000000	0 0	0 0
1	1823900	0 0	0 0
1	2291680	1 0	1 0
1	2390331	0 0	0 0
1	2490898	0 0	1 0
1	2830551	0 0	0.99591 0
1	3044306	0 0	0.12257 0
1	3074306	0 0	0 0
1	3197747	0 0	0 0
```

```
$ lancestools mod -c 0.8 example1.laf \
| lancestools vcf -a example1.vcf - \
| head

compressing with cutoff:0.8
annotating vcf according to laf
##fileformat=VCFv4.1
##FILTER=<ID=PASS,Description="All filters passed">
##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">
##FORMAT=<ID=LA,Number=1,Type=String,Description="Local Ancestry status">
##FORMAT=<ID=LA,Number=1,Type=String,Description="Local Ancestry status">
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	NA19625	NA19700	NA19701	NA19703	NA19704
1	1823922	1_1823922	A	G	999	PASS	.	GT:LA	0|0:.|0	0|1:.|0	0|1:.|0	1|1:0|0	1|0:0|0
1	2291680	1_2291680	T	C	999	PASS	.	GT:LA	1|1:2|0	1|1:2|0	1|1:2|0	1|1:0|0	1|1:0|0
1	2390331	1_2390331	G	A	999	PASS	.	GT:LA	0|1:2|0	0|0:2|0	0|0:2|0	0|0:0|0	0|0:0|0
1	2490898	1_2490898	C	A	999	PASS	.	GT:LA	1|1:2|0	1|1:2|0	0|0:2|0	1|1:0|0	1|1:0|0
```

```
lancestools comp \
    <(lancestools convert -p ../example1.fbk.txt ../example1.markers.txt -v example1.vcf | lancestools mod -c 0.99 - )\
    <(lancestools mod -c 0.8 example1.laf)

compressing with cutoff:0.99
compressing with cutoff:0.8
Comparing file /dev/fd/63 with reference file /dev/fd/62
converting pcadmix output to laf
convert --pcadmix: ancestries represented as follows:
0: ./AFR_overlap.bgl
1: ./EAS_overlap.bgl
2: ./EUR_overlap.bgl
#IND	accuracy	accuracy_classified	frac_classified	N
NA19625_A	NA	NA	NA	0
NA19625_B	0.93333	1	0.93333	15
NA19700_A	0.2	0.22222	0.9	20
NA19700_B	0.95	1	0.95	20
NA19701_A	0.2	0.22222	0.9	20
NA19701_B	0.95	1	0.95	20
NA19703_A	0.8	1	0.8	20
NA19703_B	1	1	1	20
NA19704_A	NA	NA	NA	0
NA19704_B	0	0	1	20
```

## Usage ##

The usage, which you can see by running `lancestools -h` is as follows:

```
usage: lancestools [-h] [--debug] {convert,mod,vcf,prop,comp} ...

lancestools:
Converts, merges, edits, compares, uses local admixture LAF files (and for some operations also ps21.txt files from elai). 
There are several operations available, see options below, and by combining them you can do much more.

positional arguments:
  {convert,mod,vcf,prop,comp}
                        run each of these subcommands with option -h to see their options and
                        descriptions.
    convert             Convert several formats into LAF.
    mod                 Edit LAF files (merge multiple LAF, cutoff, sort and filter individuals,
                        filter SNPs...)
    vcf                 Annotate or mask a VCF
    prop                Returns ancestry proportions.
    comp                Compare 2 LAF files.

optional arguments:
  -h, --help            show this help message and exit
  --debug               help in debugging, set traceback limit to 5

Accepts also standard input, specified as - in place of a file, and gzipped files (but not gzipped standard input).
Please verify the results as many operations have not been tested very thoroughly.

Example:
to perform an intersection you need to start from compressed values (i.e {0,1}), if you don't have compressed values use
lancestools mod -c to obtain them. Then you do an average and compress them again. If we want we can then extract only
some individuals and use this file to mask a VCF. Therefore: 

lancestools mod -m <(lancestools mod -c 0 my.laf ) <(lancestools mod -c 0 myother.laf ) \
| lancestools mod -c 0.95 - \
| lancestools mod -S mysamplelist - \
| lancestools vcf -m my.vcf - > output.vcf 
```

See below the usage for each of the lancestools submodules:

### LAncesTools convert ###

```
usage: lancestools convert [-h]
                           [-p FBK MARKERS | -r FILE | -P FILE | -m FILE | -t FILE | -T TPED TFAM]
                           [-v FILE] [-s FILE:CHR:POS:ID] [-n FILE] [-a ANCS] [-d]

Convert several formats into LAF. In many cases, SNP coordinates and sample names should be
provided with dedicated options or using a VCF as reference.

optional arguments:
  -h, --help            show this help message and exit
  -p FBK MARKERS, --pcadmix FBK MARKERS
                        Convert *.fbk files as produced by PCAdmix
                        (https://doi.org/10.3378/027.084.0401). Expects a *.fbk followed by a
                        *.markers file. SNP coordinates should also be provided.
  -r FILE, --rfmix FILE
                        Convert *.ForwardBackward.txt files as produced by RFMix_v1
                        (https://doi.org/10.1016/j.ajhg.2013.06.020). SNP coordinates should also
                        be provided.
  -P FILE, --ps21 FILE  Convert \*.ps21 files, as produced by ELAI
                        (https://doi.org/10.1534/genetics.113.160697). SNP coordinates and sample
                        names should also be provided.
  -m FILE, --matrix FILE
                        Convert matrix-like files with haplotypes on rows. Each field is the
                        ancestry at each position. Accepts {\t ,;} as field delimiter. Accepts also
                        non-delimited formats where each digit is considered a field. Snp
                        coordinates and sample names should also be provided.
  -t FILE, --tmatrix FILE
                        Convert matrix-like file with positions on rows. Each field is the ancestry
                        at each haplotype. See option -m for further details.
  -T TPED TFAM, --tped TPED TFAM
                        Convert masked *.tped. A way to encode local ancestry in *.tped files is to
                        multiply the individuals and set missing the snps where the local ancestry
                        is different. Expects a *.tped followed by a *.tfam file.
  -v FILE, --vcf FILE   VCF file with snp coordinates and sample names, necessary when input
                        formats do not have these info
  -s FILE:CHR:POS:ID, --snpinfo FILE:CHR:POS:ID
                        File with snp coordinates, necessary if they are not available from VCF or
                        input formats. The file must contain chromosome, position and snp ID in tab
                        or space-delimited columns. The position of such columns should be
                        specified in the option argument by colon-separated numbers following the
                        filename, eg: snpinfo.txt:2:3:1. Leading lines starting with # will be
                        skipped.
  -n FILE, --sampleinfo FILE
                        File with sample names, necessary if they are not available from VCF or
                        input formats. One sample name per line.
  -a ANCS, --ancs ANCS  Either a comma-separated list of ancestries present in file, or the number
                        of ancestries (in this case they will be assumed to be 0,1,2...). Necessary
                        when converting tped and non-annotated matrix-like files.
  -d                    flag the submitted VCF as doubled (i.e. containing homozygous individuals
                        representing a haplotype)
```

### LAncesTools mod ###

```
usage: lancestools mod [-h] [-m | -c CUTOFF | -s | -r STRING | -S FILE | -i FILE] [--ps21]
                       LAF_FILE [LAF_FILE ...]

Edit LAF files (merge multiple LAF, cutoff, sort and filter individuals, filter SNPs...)

positional arguments:
  LAF_FILE              input files to edit

optional arguments:
  -h, --help            show this help message and exit
  -m, --merge           Average across LAF. Applicable also to \*.ps21 files.
  -c CUTOFF, --cutoff CUTOFF
                        Compress LAF into {0,1} values picking the maximum ancestry, with cutoff
                        NUM. if even the maximum ancestry is below the cutoff then the result is
                        unknown
  -s, --sort_cols       sort columns lexicografically, very important before merging/comparing
                        files with different origin
  -r STRING, --reord_ancs STRING
                        reorder ancestries according the new position given in the string: e.g.
                        3,1,2 put the third ancestry as first, the first as second and the second
                        as third.
  -S FILE, --sample_list FILE
                        extracts from file only the samples in FILE (one column, one sample per
                        row)
  -i FILE, --interpolate FILE
                        Return a file with the ancestry information for the snps in FILE,
                        subsetting or interpolating the input file. FILE should have 2 tab or
                        space-delimited columns, chromosome and position. Multi-chromosome
                        interpolation is not implemented yet.
  --ps21                The input file are \*.ps21 files and not LAF, valid only for options --merge
                        and --cutoff
```
                        
### LAncesTools vcf ###

```
usage: lancestools vcf [-h] [-m VCF_FILE | -a VCF_FILE] LAF_FILE

Annotate or mask a VCF

positional arguments:
  LAF_FILE              input LAF

optional arguments:
  -h, --help            show this help message and exit
  -m VCF_FILE, --mask VCF_FILE
                        Returns a masked version of VCF_FILE according to the LAF file in argument.
                        The two haplotypes of each individual need to be present independently in
                        the LAF file with suffixes _A or _B.
  -a VCF_FILE, --annotate VCF_FILE
                        Returns an annotated version of VCF_FILE according to the LAF file in
                        argument. The two haplotypes of each individual need to be present
                        independently in the LAF file with suffixes _A or _B. The annotation
                        consists in an extra subfield in the genotype fields of all individuals,
                        specified in the format as LA.
```

### LAncesTools prop ###

```
usage: lancestools prop [-h] [-g | -l] LAF_FILE [LAF_FILE ...]

Returns ancestry proportions. Computes for each individual the global ancestry proportions as snp
fraction belonging to each ancestry or the site-wise local ancestry proportion across haplotypes.
Accepts multiple files with same individuals treating them as independent positions (e.g. multiple
chromosomes).

positional arguments:
  LAF_FILE    input files

optional arguments:
  -h, --help  show this help message and exit
  -g, --gap   Computes for each individual the sample-wise global ancestry proportions as snp
              fraction belonging to each ancestry.
  -l, --lap   Computes for each snp the site-wise local ancestry proportions as haplotypic fraction
              belonging to each ancestry.
```

### LAncesTools comp ###

```
usage: lancestools comp [-h] LAF_FILE LAF_FILE

Compare 2 LAF files. This comparison uses classification statistics and it is only meant for non-
continuous (0,1) ancestry classifications. The first file is meant to be the classification while
the second is the truth. All measures are SNP-based. The classified accuracy is computed as the
count of true positives over all classified SNPs (i.e. which are not unknown). When comparing two
different classifications, an unknown SNP in any of the 2 files will be considered as non
classified, and will not count towards the classified accuracy.

positional arguments:
  LAF_FILE    file to compare

optional arguments:
  -h, --help  show this help message and exit
```
